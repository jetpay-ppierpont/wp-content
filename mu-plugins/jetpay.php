<?php
function cptui_register_my_cpts()
{

  /**
   * Post Type: News Articles.
   */

  $labels = array(
    "name" => __("News Articles", "custom-post-type-ui"),
    "singular_name" => __("News Article", "custom-post-type-ui"),
    "menu_name" => __("News Article", "custom-post-type-ui"),
    "all_items" => __("All News Articles", "custom-post-type-ui"),
    "add_new" => __("Add News Article", "custom-post-type-ui"),
    "add_new_item" => __("Add News Article", "custom-post-type-ui"),
    "edit_item" => __("Edit News Article", "custom-post-type-ui"),
    "new_item" => __("New News Article", "custom-post-type-ui"),
    "view_item" => __("View News Article", "custom-post-type-ui"),
    "view_items" => __("View News Article", "custom-post-type-ui"),
    "search_items" => __("Search News Article", "custom-post-type-ui"),
    "name_admin_bar" => __("News Article", "custom-post-type-ui"),
  );

  $args = array(
    "label" => __("News Articles", "custom-post-type-ui"),
    'show_in_graphql' => true,
    'graphql_single_name' => 'newsArticle',
    'graphql_plural_name' => 'newsArticles',
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "news",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => "news",
    "show_in_menu" => "edit.php?post_type=blog",
    "show_in_nav_menus" => true,
    "exclude_from_search" => false,
    "capability_type" => "news",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array("slug" => "news", "with_front" => true),
    "query_var" => true,
    "supports" => array("title", "editor", "excerpt"),
  );

  register_post_type("news", $args);

  /**
   * Post Type: Blogs.
   */

  $labels = array(
    "name" => __("Blogs", "custom-post-type-ui"),
    "singular_name" => __("Blog", "custom-post-type-ui"),
    "menu_name" => __("Blog", "custom-post-type-ui"),
    "all_items" => __("Blogs", "custom-post-type-ui"),
    "add_new" => __("Add Blog", "custom-post-type-ui"),
    "add_new_item" => __("Add New Blog Post", "custom-post-type-ui"),
    "edit_item" => __("Edit Blog", "custom-post-type-ui"),
    "new_item" => __("New Blog", "custom-post-type-ui"),
    "view_item" => __("View Blog", "custom-post-type-ui"),
    "view_items" => __("View Blogs", "custom-post-type-ui"),
    "search_items" => __("Search Blogs", "custom-post-type-ui"),
    "archives" => __("Blog Archives", "custom-post-type-ui"),
    "insert_into_item" => __("Insert into blog", "custom-post-type-ui"),
    "items_list_navigation" => __("Blogs list navigation", "custom-post-type-ui"),
    "items_list" => __("Blogs list", "custom-post-type-ui"),
    "attributes" => __("Blog Attributes", "custom-post-type-ui"),
    "name_admin_bar" => __("Blog", "custom-post-type-ui"),
  );

  $args = array(
    "label" => __("Blogs", "custom-post-type-ui"),
    'show_in_graphql' => true,
    'graphql_single_name' => 'blog',
    'graphql_plural_name' => 'blogs',
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "blog",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => "blog",
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "exclude_from_search" => false,
    "capability_type" => "blog",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array("slug" => "blog", "with_front" => true),
    "query_var" => true,
    "menu_icon" => "dashicons-media-text",
    "supports" => array("title", "editor", "author", "excerpt"),
  );

  register_post_type("blog", $args);

  /**
   * Post Type: Announcements.
   */

  $labels = array(
    "name" => __("Announcements", "custom-post-type-ui"),
    "singular_name" => __("Announcement", "custom-post-type-ui"),
    "menu_name" => __("Announcements", "custom-post-type-ui"),
    "all_items" => __("All Announcements", "custom-post-type-ui"),
    "add_new" => __("Add Announcement", "custom-post-type-ui"),
    "add_new_item" => __("Add Announcement", "custom-post-type-ui"),
    "edit_item" => __("Edit Announcement", "custom-post-type-ui"),
    "new_item" => __("New Announcement", "custom-post-type-ui"),
    "view_item" => __("View Announcement", "custom-post-type-ui"),
    "view_items" => __("View Announcement", "custom-post-type-ui"),
    "archives" => __("Announcement Archive", "custom-post-type-ui"),
    "insert_into_item" => __("Insert into Announcement", "custom-post-type-ui"),
    "uploaded_to_this_item" => __("Uploaded to this Announcement", "custom-post-type-ui"),
    "items_list" => __("Announcement List", "custom-post-type-ui"),
    "attributes" => __("Announcement Attributes", "custom-post-type-ui"),
    "name_admin_bar" => __("Announcement", "custom-post-type-ui"),
  );

  $args = array(
    "label" => __("Announcements", "custom-post-type-ui"),
    'show_in_graphql' => true,
    'graphql_single_name' => 'announcement',
    'graphql_plural_name' => 'announcements',
    "labels" => $labels,
    "description" => "This should be used for announcing things across the website. We\'ll display the most recent announcements all across JetPay.com. ",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "announcement",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => true,
    "show_in_menu" => "edit.php?post_type=blog",
    "show_in_nav_menus" => true,
    "exclude_from_search" => true,
    "capability_type" => "announcement",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array("slug" => "announcement", "with_front" => true),
    "query_var" => true,
    "supports" => array("title", "editor", "excerpt"),
  );

  register_post_type("announcement", $args);

  /**
   * Post Type: Products.
   */

  $labels = array(
    "name" => __("Products", "custom-post-type-ui"),
    "singular_name" => __("Product", "custom-post-type-ui"),
    "menu_name" => __("Our Products", "custom-post-type-ui"),
    "all_items" => __("Products", "custom-post-type-ui"),
    "add_new" => __("Add Product", "custom-post-type-ui"),
    "add_new_item" => __("Add New Product", "custom-post-type-ui"),
    "edit_item" => __("Edit Product", "custom-post-type-ui"),
    "new_item" => __("New Product", "custom-post-type-ui"),
    "view_item" => __("View Product", "custom-post-type-ui"),
    "view_items" => __("View Products", "custom-post-type-ui"),
    "search_items" => __("Search Products", "custom-post-type-ui"),
    "not_found" => __("No Products Found", "custom-post-type-ui"),
    "not_found_in_trash" => __("No Products found in trash", "custom-post-type-ui"),
    "insert_into_item" => __("Insert into Product", "custom-post-type-ui"),
    "name_admin_bar" => __("Product", "custom-post-type-ui"),
  );

  $args = array(
    "label" => __("Products", "custom-post-type-ui"),
    'show_in_graphql' => true,
    'graphql_single_name' => 'product',
    'graphql_plural_name' => 'products',
    "labels" => $labels,
    "description" => "Top Level Products at JetPay (e.g. JetSource)",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "solutions",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => false,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "exclude_from_search" => false,
    "capability_type" => "product",
    "map_meta_cap" => true,
    "hierarchical" => true,
    "rewrite" => array("slug" => "solutions", "with_front" => true),
    "query_var" => true,
    "menu_icon" => "dashicons-hammer",
    "supports" => array("title", "editor", "excerpt"),
  );

  register_post_type("solutions", $args);

  /**
   * Post Type: Modules.
   */

  $labels = array(
    "name" => __("Modules", "custom-post-type-ui"),
    "singular_name" => __("Module", "custom-post-type-ui"),
    "menu_name" => __("Modules", "custom-post-type-ui"),
    "all_items" => __("All Product Modules", "custom-post-type-ui"),
    "add_new" => __("Add Module", "custom-post-type-ui"),
    "add_new_item" => __("Add New Module", "custom-post-type-ui"),
    "edit_item" => __("Edit Module", "custom-post-type-ui"),
    "new_item" => __("New Module", "custom-post-type-ui"),
    "view_item" => __("View Module", "custom-post-type-ui"),
    "view_items" => __("View Modules", "custom-post-type-ui"),
    "search_items" => __("Search Modules", "custom-post-type-ui"),
    "not_found" => __("No Modules Found", "custom-post-type-ui"),
    "name_admin_bar" => __("Product Module", "custom-post-type-ui"),
  );

  $args = array(
    "label" => __("Modules", "custom-post-type-ui"),
    'show_in_graphql' => true,
    'graphql_single_name' => 'module',
    'graphql_plural_name' => 'modules',
    "labels" => $labels,
    "description" => "Modules are smaller options or features of main products offered by JetPay.",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "modules",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => false,
    "show_in_menu" => "edit.php?post_type=solutions",
    "show_in_nav_menus" => true,
    "exclude_from_search" => false,
    "capability_type" => "module",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array("slug" => "modules", "with_front" => true),
    "query_var" => true,
    "supports" => array("title", "editor", "excerpt"),
  );

  register_post_type("modules", $args);

  /**
   * Post Type: Partners.
   */

  $labels = array(
    "name" => __("Partners", "custom-post-type-ui"),
    "singular_name" => __("Partner", "custom-post-type-ui"),
    "menu_name" => __("Partners", "custom-post-type-ui"),
    "all_items" => __("Partners", "custom-post-type-ui"),
    "add_new" => __("Add Partner", "custom-post-type-ui"),
    "add_new_item" => __("Add New Partner", "custom-post-type-ui"),
    "edit_item" => __("Edit Partner", "custom-post-type-ui"),
    "new_item" => __("New Partner", "custom-post-type-ui"),
  );

  $args = array(
    "label" => __("Partners", "custom-post-type-ui"),
    'show_in_graphql' => true,
    'graphql_single_name' => 'partner',
    'graphql_plural_name' => 'partners',
    "labels" => $labels,
    "description" => "Partners of JetPay",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "partner",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => false,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "exclude_from_search" => false,
    "capability_type" => "partner",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array("slug" => "partner", "with_front" => true),
    "query_var" => true,
    "menu_icon" => "dashicons-universal-access",
    "supports" => array("title", "editor", "thumbnail", "excerpt"),
  );

  register_post_type("partner", $args);

  /**
   * Post Type: Docs.
   */

  $labels = array(
    "name" => __("Docs", "custom-post-type-ui"),
    "singular_name" => __("Documentation Page", "custom-post-type-ui"),
    "menu_name" => __("Product Docs", "custom-post-type-ui"),
    "all_items" => __("All Product Docs", "custom-post-type-ui"),
    "add_new" => __("Add Documentation Page", "custom-post-type-ui"),
    "add_new_item" => __("Add New Documentation Page", "custom-post-type-ui"),
    "edit_item" => __("Edit Documentation Page", "custom-post-type-ui"),
  );

  $args = array(
    "label" => __("Docs", "custom-post-type-ui"),
    'show_in_graphql' => true,
    'graphql_single_name' => 'doc',
    'graphql_plural_name' => 'docs',
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "docs",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "exclude_from_search" => false,
    "capability_type" => "documentation",
    "map_meta_cap" => true,
    "hierarchical" => true,
    "rewrite" => array("slug" => "docs", "with_front" => true),
    "query_var" => true,
    "menu_icon" => "dashicons-media-code",
    "supports" => array("title", "editor", "excerpt"),
  );

  register_post_type("documentation", $args);

  /**
   * Post Type: Announcements.
   */

  $labels = array(
    "name" => __("Announcements", "custom-post-type-ui"),
    "singular_name" => __("Announcement", "custom-post-type-ui"),
    "menu_name" => __("Announcements", "custom-post-type-ui"),
    "all_items" => __("All Announcements", "custom-post-type-ui"),
    "add_new" => __("Add Announcement", "custom-post-type-ui"),
    "add_new_item" => __("Add Announcement", "custom-post-type-ui"),
    "edit_item" => __("Edit Announcement", "custom-post-type-ui"),
    "new_item" => __("New Announcement", "custom-post-type-ui"),
    "view_item" => __("View Announcement", "custom-post-type-ui"),
    "view_items" => __("View Announcement", "custom-post-type-ui"),
    "archives" => __("Announcement Archive", "custom-post-type-ui"),
    "insert_into_item" => __("Insert into Announcement", "custom-post-type-ui"),
    "uploaded_to_this_item" => __("Uploaded to this Announcement", "custom-post-type-ui"),
    "items_list" => __("Announcement List", "custom-post-type-ui"),
    "attributes" => __("Announcement Attributes", "custom-post-type-ui"),
    "name_admin_bar" => __("Announcement", "custom-post-type-ui"),
  );

  $args = array(
    "label" => __("Announcements", "custom-post-type-ui"),
    'show_in_graphql' => true,
    'graphql_single_name' => 'announcement',
    'graphql_plural_name' => 'announcements',
    "labels" => $labels,
    "description" => "This should be used for announcing things across the website. We\'ll display the most recent announcements all across JetPay.com. ",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "announcement",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => true,
    "show_in_menu" => "edit.php?post_type=blog",
    "show_in_nav_menus" => true,
    "exclude_from_search" => true,
    "capability_type" => "announcement",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array("slug" => "announcement", "with_front" => true),
    "query_var" => true,
    "supports" => array("title", "editor", "excerpt"),
  );

  register_post_type("announcement", $args);

  /**
   * Post Type: Change Log.
   */

  $labels = array(
    "name" => __("changelog", "custom-post-type-ui"),
    "singular_name" => __("Change Log", "custom-post-type-ui"),
    "menu_name" => __("Change Logs", "custom-post-type-ui"),
    "all_items" => __("All Change Logs", "custom-post-type-ui"),
    "add_new" => __("Add Change Logs", "custom-post-type-ui"),
    "add_new_item" => __("Add Change Logs", "custom-post-type-ui"),
    "edit_item" => __("Edit Change Logs", "custom-post-type-ui"),
    "new_item" => __("New Change Logs", "custom-post-type-ui"),
    "view_item" => __("View Change Logs", "custom-post-type-ui"),
    "view_items" => __("View Change Logs", "custom-post-type-ui"),
    "archives" => __("Change Logs Archive", "custom-post-type-ui"),
    "insert_into_item" => __("Insert into Change Logs", "custom-post-type-ui"),
    "uploaded_to_this_item" => __("Uploaded to this Change Logs", "custom-post-type-ui"),
    "items_list" => __("Change Log List", "custom-post-type-ui"),
    "attributes" => __("Change Log Attributes", "custom-post-type-ui"),
    "name_admin_bar" => __("Change Log", "custom-post-type-ui"),
  );

  $args = array(
    "label" => __("Change Logs", "custom-post-type-ui"),
    'show_in_graphql' => true,
    'graphql_single_name' => 'changeLog',
    'graphql_plural_name' => 'changeLogs',
    "labels" => $labels,
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "changelog",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "exclude_from_search" => true,
    "capability_type" => "changelog",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array("slug" => "changelog", "with_front" => true),
    "query_var" => true,
    "menu_icon" => "dashicons-list-view",
    "supports" => array("title", "excerpt"),
  );

  register_post_type("changelog", $args);

  $labels = array(
    "name" => __("leadership", "custom-post-type-ui"),
    "singular_name" => __("Executive Bio", "custom-post-type-ui"),
    "menu_name" => __("Executive Bios", "custom-post-type-ui"),
    "all_items" => __("All Executive Bios", "custom-post-type-ui"),
    "add_new" => __("Add Executive Bio", "custom-post-type-ui"),
    "add_new_item" => __("Add New Executive Bio", "custom-post-type-ui"),
    "edit_item" => __("Edit Executive Bio", "custom-post-type-ui"),
  );

  $args = array(
    "label" => __("Executive Bio", "custom-post-type-ui"),
    'show_in_graphql' => true,
    'graphql_single_name' => 'executiveBio',
    'graphql_plural_name' => 'executiveBios',
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "leadership",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => false,
    "exclude_from_search" => false,
    "capability_type" => "executive",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array("slug" => "leadership", "with_front" => true),
    "query_var" => true,
    "menu_icon" => "dashicons-admin-users",
    "supports" => array("excerpt"),
  );

  register_post_type("leadership", $args);

  /**
   * Post Type: Office Status.
   */

  $labels = array(
    "name" => __("office_status", "custom-post-type-ui"),
    "singular_name" => __("Office Status", "custom-post-type-ui"),
    "menu_name" => __("Office Status", "custom-post-type-ui"),
    "all_items" => __("Office Statuses", "custom-post-type-ui"),
    "add_new" => __("New Office Status", "custom-post-type-ui"),
    "add_new_item" => __("Add Office Status", "custom-post-type-ui"),
    "edit_item" => __("Edit Office Status", "custom-post-type-ui"),
    "new_item" => __("New Office Status", "custom-post-type-ui"),
    "view_item" => __("View Office Status", "custom-post-type-ui"),
    "view_items" => __("View Office Status", "custom-post-type-ui"),
    "archives" => __("Office Status Archive", "custom-post-type-ui"),
    "insert_into_item" => __("Insert into Office Status", "custom-post-type-ui"),
    "uploaded_to_this_item" => __("Uploaded to this Office Status", "custom-post-type-ui"),
    "items_list" => __("Office Status List", "custom-post-type-ui"),
    "attributes" => __("Office Status Attributes", "custom-post-type-ui"),
    "name_admin_bar" => __("Office Status", "custom-post-type-ui"),
  );

  $args = array(
    "label" => __("Office Status", "custom-post-type-ui"),
    'show_in_graphql' => true,
    'graphql_single_name' => 'officeStatus',
    'graphql_plural_name' => 'officeStatuses',
    "labels" => $labels,
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "office_status",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "exclude_from_search" => true,
    "capability_type" => "office_status",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array("slug" => "office_status", "with_front" => true),
    "query_var" => true,
    "menu_icon" => "dashicons-building",
    "supports" => array("title", "editor", "excerpt"),
  );

  register_post_type("office_status", $args);

}

add_action('init', 'cptui_register_my_cpts');

// natalie.sabre -- Natalie@JetPay2019
// michelle.jenkins -- JetPay2019
// dave.lantz -- Dave@JetPay2019
// parker.pierpont ****
