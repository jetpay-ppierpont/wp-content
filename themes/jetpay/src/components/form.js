import Vue from "vue";
import search from "./search.js";
import axios from "axios";

const postId = wpId;
const postType = wpPostType;
const location = window.location.href;

global.app = new Vue({
  el: "#signup-form",
  data: {
    formId: wpFormId,
    successResponse: `${wpFormEntryResponse}`,
    failResponse: "Please double check your entries.",
    portalId: 4918937,
    postType: postType,
    postId: postId,
    showFailMessage: false,
    showSuccessMessage: false,
    isLoaded: false,
    formFields: [],
    requestUrl: `${wpSite}/wp-json/jetpay/v1/data?form=${wpFormId}`
  },
  created: function() {
    this.loadForm();
  },
  methods: {
    loadForm: function() {
      axios
        .get(`${this.requestUrl}`)
        .then(function(response) {
          response.data.map(field => {
            const newField = {
              name: field.name,
              label: field.label,
              type: field.name == "email" ? "email" : `${field.fieldType}`,
              required: field.required,
              placeholder: field.placeholder,
              value: null
            };
            app.formFields.push(newField);
          });
        })
        .then(function() {
          app.isLoaded = true;
        })
        .catch(function(error) {});
    },
    submit: function() {
      // the request generator function puts together all our form-bound data for us!
      const request = requestGenerator(this.formFields);
      console.log(request);
      axios
        .post(
          `https://api.hsforms.com/submissions/v3/integration/submit/2004225/${wpFormId}`,
          request
        )
        .then(function(response) {
          onSuccess(response);
        })
        .catch(function(error) {
          onFail(error);
        });
    }
  }
});

const onFail = () => {
  app.showSuccessMessage = false;
  app.showFailMessage = true;
};

const onSuccess = error => {
  console.log(error.message);
  app.showFailMessage = false;
  app.showSuccessMessage = true;
  setTimeout(() => {
    app.isLoaded = false;
  }, 5000);
};

const requestGenerator = fields => {
  const fieldsArr = [];

  fields.map(field => {
    const keyValPair = {
      name: `${field.name}`,
      value: `${field.value}`
    };
    fieldsArr.push(keyValPair);
  });

  const body = {
    submittedAt: `${Date.now()}`,
    fields: [...fieldsArr],
    context: {
      pageUri: `${window.location.href}`,
      pageName: `${wpPageTitle}`
    },
    skipValidation: false
  };

  return body;
};
