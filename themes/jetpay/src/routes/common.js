export default {
	init() {
		// JavaScript to be fired on all pages
		console.log('common');

		// if ("serviceWorker" in navigator) {
		//   // Use the window load event to keep the page load performant
		//   window.addEventListener("load", () => {
		//     navigator.serviceWorker.register("/serviceworker.js");
		//   });
		// }
	},
	finalize() {
		const modals = document.querySelectorAll('.modal__content[modal]');
		const triggers = document.querySelectorAll('[modal_trigger]');
		const overlay = document.querySelector('.modal__overlay');
		const body = document.querySelector('body');
		const modalLib = [];

		const getHeight = (theModal) => {
			const windowHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
			const windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
			const modalHeight = theModal.modal.clientHeight;
			const modalWidth = theModal.modal.clientWidth;
			const topValue = (windowHeight - modalHeight) / 2;
			const leftValue = (windowWidth - modalWidth) / 2;
			return { top: topValue, left: leftValue };
		};

		const getModalObj = (t) => {
			const mName = t.attributes.modal_trigger.value;
			const mTrigger = t;
			const m = document.querySelector(`[modal="${mName}"]`);
			const mObj = { name: mName, modal: m, trigger: mTrigger };
			const positionValues = getHeight(mObj);
			m.style.top = `${positionValues.top}px`;
			m.style.left = `${positionValues.left}px`;
			return { ...mObj };
		};

		const openModal = (e) => {
			if (!body.classList.contains('modal')) {
				// define the modal, it's trigger, and top value into
				// an object
				const theModal = getModalObj(e.target);
				// add modal classList to body;
				body.classList.add('modal');
				// add modal attribute w/ name of modal to overlay
				overlay.setAttribute('modal', `"${theModal.name}"`);
				// remove 'hide' class from modal.set delay so we
				// wait on the overlay to open.
				setTimeout(function() {
					theModal.modal.classList.remove('hide');
				}, 300);

				console.log(theModal.modal.style.top);
			} else {
				return null;
			}
		};

		const closeModal = (e) => {
			if (body.classList.contains('modal')) {
				const mName = e.target.attributes.modal.value;
				// find the open modal
				const openModal = document.querySelector(`[modal=${mName}]`);
				// add the hide attribute to open modal
				openModal.classList.add('hide');
				// remove modal attribute from overlay
				e.target.removeAttribute('modal');
				// add delay to closing the overlay so we can wait
				// on the modal to close first.
				setTimeout(function() {
					body.classList.remove('modal');
				}, 300);
			}
		};

		triggers.forEach((trigger) => trigger.addEventListener('mousedown', openModal));

		overlay.addEventListener('mousedown', closeModal);
	}
};
