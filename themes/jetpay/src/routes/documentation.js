import Swup from "swup";
import Vue from "vue";
import axios from "axios";

export default {
  init() {
    console.log("documentation");
  },
  finalize() {
    /** Page Transition Effects **/

    global.searchList = new Vue({
      el: "#content-header",
      data: {
        showModal: null,
        requestUrl: `${wpSite}/wp-json/wp/v2/docs?search=`,
        query: "",
        results: [],
        show: null
      },
      methods: {
        search: function(id) {
          axios
            .get(`${this.requestUrl}"${this.query}"`)
            .then(function(response) {
              searchList.results = response.data;
              console.log(searchList.results);
            })
            .catch(function(error) {
              console.log(error);
            });
        },
        showMe: function() {
          this.show = true;
        },
        hideMe: function() {
          this.show = null;
        },
        focusSearch: function() {
          this.show = true;
          document.querySelector("input.seach-input").focus();
        },
        blurSearch: function() {
          this.show = null;
          document.querySelector("input.seach-input").blur();
        },
        goTo: function(link) {
          // console.log(link);
          window.location.href = link;
        }
      }
    });
  }
};
