import hrp from "./hrp.png";
import jetsource from "./jetsource.png";
import jetX from "./jetx.png";
import logo from "./logo.png";
import partner from "./partner.png";
import payments from "./payments.png";
import terminal from "./terminal.png";

module.exports = {
  hrp,
  jetsource,
  jetX,
  logo,
  partner,
  payments,
  terminal
};
