<?php
getHeader();

?>
<div>
  <?php

while (have_posts()) {
  the_post();
  $category = get_field('category');
  productHeader($category);
  ?>

  <div class="page-builder">
    <?php the_content() ?>
  </div>

  <?php

} ?>

</div>

<?php
getFooter();