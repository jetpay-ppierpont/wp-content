<?php

/*
 **
 * Template Name: JetPay - (Elementor - Light Secondary Header)
 * Template Post Type: modules, solutions
 **
 */

jpHeader(array(
  'color' => 'lightSecondary',
  'linkColor' => '#c6c6cb',
));

while (have_posts()) {
  the_post();

  ?><div style="margin-top:-105px"><?php the_content(); ?></div>
  <?php

}



if (get_field('show_form')) getForm();



/*
 ** Notice -- custom footer function
 *	$args of jpFooter() can be color, mono,
 *	orange, teal, or blue.
 **
 */

jpFooter(array('color' => 'color'));