<?php
getDocsHeader();
?>

      <section id="page-content" class="swup-doc-single">
        <?php while (have_posts()) : the_post(); ?>

        <header class="page-header">
         <h2 class="page-title"><?php the_title(); ?></h2>
         <p class="page-summary"><?php the_field('summary'); ?></p>
        </header>

        <article class="documentation-content">
          <?php the_content(); ?>
        </article>

        <hr class="between-articles">

        <?php endwhile; ?>
      </section>


<?php
getDocsFooter(); ?>