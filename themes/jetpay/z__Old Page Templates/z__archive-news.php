<?php
jpHeader(array('color' => 'darkSecondary'));
?>

<div class="blog-archive container">
  <h1 class="blog-header__title color--dark-secondary">News</h1>
  <div class="blog-archive__container grid">

    <?php
    $i = 1;
    while (have_posts()) {
      the_post();
      $featured = get_field('featured');
      ?>
      <div class="card rounded <?php echo $featured ?>"  <?php if ($featured) { echo 'style="grid-column: span 2;"';} ?>>
        <div class="blog-preview__text-container">
        <?php if ($featured) { ?>
          <h6 class="color--blue900 news-headline">
            Featured
          </h6>
          <a class="color--dark-primary" href="<?php the_permalink(); ?>">
            <h5 style="font-size: 24px;">
              <?php the_title(); ?>
            </h5>
          </a>
              <?php

            } else { ?>
            <a class="color--gray700" href="<?php the_permalink(); ?>">
              <h6 class="color--grey700 news-headline"><?php the_title(); ?></h6>
            </a>
                <?php

              } ?>
          <div class="excerpt">
            <?php if ($featured) {
              echo "<p>" . excerpt(50) . "</p>";;
            } else {
              echo "<p>" . excerpt(25) . "</p>";;
            }
            ?>
          <p class="caption"><?php echo get_the_date() ?></p>
          </div>
        </div>
      </div>

      <?php

    }
    ?>
  </div>
</div>
<div class="link-pagination">
  <?php echo paginate_links(); ?>
</div>
<?php
// Let's get the footer
jpFooter(array('color' => 'mono'));