<?php

/*
 **
 * Template Name: JetPay - (Elementor - Normal Header (home) )
 **
 */

jpHeader(array(
  'color' => 'lightPrimary',
  'linkColor' => '#fff',
));

getNews();

while (have_posts()) {
  the_post();

  ?><div style="margin-top:-105px"><?php the_content(); ?></div>
  <?php

}



if (get_field('show_form')) getForm();



/*
 ** Notice -- custom footer function
 *	$args of jpFooter() can be color, mono,
 *	orange, teal, or blue.
 **
 */

jpFooter(array('color' => 'teal'));