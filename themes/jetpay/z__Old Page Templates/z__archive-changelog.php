<?php jpHeader(array('color' => 'darkSecondary')); ?>
<div class="blog-archive container">
  <h1 class="changelog-header">Release Notes</h1>
  <div class="changelog-group">
  <?php
  while (have_posts()) {
    the_post();
    $product = getTitle(get_field('product')['0']);
    $date = get_field('date');

    headerDate($wp_query);
    ?>
    <div class="changelog-container">
      <article class="changelog-entry">
        <a class="changelog normal-link" href="<?php the_permalink() ?>">
          <p class="caption"><?php echo date("m.d", strtotime($date)); ?></p>
          <p class="changelog-entry-title overline"><?php echo $product; ?></p>
          <p class="caption changelog-entry-summary"><?php echo get_field('brief_summary'); ?></p>

            <p class="view-changelog caption"><i class="changelog far fa-external-link"></i></p>
          </a>

        </article>
    </div>

  <?php

} ?>
  </div>
</div>
</div>

<?php
jpFooter(array('color' => 'color'));
