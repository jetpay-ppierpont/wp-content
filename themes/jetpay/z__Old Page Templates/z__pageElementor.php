<?php

/*
 **
 * Template Name: JetPay - (Elementor - Normal Header)
 **
 */

getHeader(array(
  'color' => 'darkPrimary',
  'linkColor' => '#333',
));

while (have_posts()) {
  the_post();

  ?><div class="page-builder"><?php the_content(); ?></div>
  <?php

}



if (get_field('show_form')) getForm();



/*
 ** Notice -- custom footer function
 *	$args of jpFooter() can be color, mono,
 *	orange, teal, or blue.
 **
 */

jpFooter(array('color' => 'teal'));