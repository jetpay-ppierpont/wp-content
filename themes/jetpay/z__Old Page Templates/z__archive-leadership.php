<?php jpHeader(array('color' => 'darkSecondary')); ?>
<div class="blog-archive container">
  <h1 class="changelog-header">Our Leadership</h1>
    <div id="leadership">
      <div class="leadership-group grid">
      <?php
      while (have_posts()) {
        the_post();
        $name = get_field('exec_name');
        $title = get_field('exec_position');
        $photo = get_field('exec_headshot');
        $bio = get_field('exec_bio');
        ?>
          <div class="centered">
            <a id="show-modal" v-on:click="open('<?php echo get_the_ID() ?>')">
              <img src="<?php echo $photo ?>" />
              <h6><?php echo $name ?></h6>
              <p class="caption"><?php echo $title ?></p>
            </a>
          </div>
        <?php

      } ?>

        </div>
          <transition name="modal">

          </transition>
        </div>
      </div>
    </div>
  </div>
</div>


<?php

jpFooter(array('color' => 'mono'));
