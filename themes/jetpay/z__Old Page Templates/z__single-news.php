<?php

while (have_posts()) {
  the_post();
  jpHeader(array(
    'openGraph' => getFacebookTags(get_the_ID()),
    'color' => 'darkSecondary',
    'linkColor' => '#7e7e83'
  ));
  ?>
  <div class="container" style="margin-top:80px;">
  <div class="back-to-all-news">
    <a id="back-to-blog container" href="<?php echo get_post_type_archive_link('news') ?>">
      <p>
        <i class="far fa-long-arrow-left"></i>
        All News
      </p>
    </a>
  </div>
    <!-- Background-Image -->
    <div class="news-single__header-image">
      <div class="news-single__header-area-container">
        <div class="news-single__header-area">
          <h1 class="page-title">
            <?php the_title(); ?>
          </h1>
          <?php if (get_field('subtitle')) ?>
          <h2><?php echo get_field('subtitle'); ?></h2>
          <p class="caption color--light-secondary"><?php the_date() ?></p>
        </div>
      </div>
    </div>
    <!-- Header Area -->
    <article>
      <div class="news-single__content-container">
        <?php the_content(); ?>
        <?php if (get_field('show_form')) {
          ?>
          <div class="blog-single__bottom-form-container">
            <?php getForm(array('title' => "Subscribe to get the latest updates!", "content" => "blog")); ?>
          </div>
          <?php

        } ?>
      </div>
      <div class="blog-single__sidebar-container">

    <?php getSocialWidget($buttonColor); ?>

    </div>
    </article>
	</div>


<?php

}

/*
 ** Notice -- custom footer function
 *	$args of jpFooter() can be color, mono,
 *	orange, teal, or blue.
 **
 */
jpFooter(array('color' => 'gray'));