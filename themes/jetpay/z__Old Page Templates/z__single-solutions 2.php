<?php getHeader(array('color' => 'darkSecondary')); ?>
<div class="blog-archive container">
<?php

while (have_posts()) {
  the_post(); ?>
  <div>
    <article class="container">
      <?php the_content() ?>
    </article>
  </div>

<?php

} ?>

</div>

<?php
getFooter();
