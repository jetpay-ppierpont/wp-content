<?php
$category = null;
$product = null;
$buttonColor = null;
while (have_posts()) {
  the_post();
  jpHeader(array(
    'openGraph' => getFacebookTags(get_the_ID()),
    'color' => 'lightPrimary',
    'linkColor' => '#ffffff'
  ));
  $category = get_field('blog_category');
  $product = get_field('related_product');
  $buttonColor = get_field('button_color');
  ?>
	<div class="container">
    <a id="back-to-blog" href="<?php echo get_post_type_archive_link('blog') ?>">
      <p>
      <i class="far fa-long-arrow-left"></i>
      All Blogs</p>
    </a>
    <!-- Background-Image -->
    <div
      class="blog-single__header-image"
      style="background: linear-gradient(180deg, rgba(0, 0, 0, 0.3) 0%, rgba(255, 255, 255, 0) 39.56%), linear-gradient(180deg, rgba(0, 0, 0, 0) 24.73%, rgba(0, 0, 0, 0.1) 60.71%, rgba(0, 0, 0, 0.2) 74.36%, rgba(0, 0, 0, 0.4) 100%), url('<?php echo get_field('featured_image'); ?>'); background-size: cover;"
      >
      <div class="blog-single__header-area-container">
        <div class="blog-single__header-area">
          <h1 class="page-title">
            <?php the_title(); ?>
          </h1>
          <?php if (get_field('subtitle')) ?>
          <p title="author-name" class="color--light-primary"><?php echo get_field('author_name'); ?></p>

          <p class="caption color--light-primary"><?php the_date() ?></p>
        </div>
      </div>
    </div>
    <!-- End of Header Area -->
    <article>
      <div class="blog-single__content-container">
        <?php the_content(); ?>
        <?php if (get_field('show_form')) {
          ?>
          <div class="blog-single__bottom-form-container">
            <?php getForm(array('title' => "Subscribe to get the latest updates!", "content" => "blog")); ?>
          </div>
          <?php

        } ?>
      </div>

<?php
wp_reset_postdata();
}

?>
    <div class="blog-single__sidebar-container">

    <?php getSocialWidget($buttonColor); ?>
    <?php getRelatedBlogs($category[0], $product[0], get_the_ID()); ?>

    </div>
  </article>
</div>

<?php

/*
 ** Notice -- custom footer function
 *	$args of jpFooter() can be color, mono,
 *	orange, teal, or blue.
 **
 */
jpFooter(array('color' => 'color'));