<?php jpHeader(array('openGraph' => getFacebookTags()));

while (have_posts()) {
  the_post(); ?>
	<div class="container card">
    <!-- Background-Image -->
    <div class="news-single__header-image">
      <div class="news-single__header-area-container">
        <div class="news-single__header-area">
          <h1 class="page-title">
            <?php the_title(); ?>
          </h1>
          <?php if (get_field('subtitle')) ?>
          <h2><?php echo get_field('subtitle'); ?></h2>
          <p class="caption color--light-secondary"><?php the_date() ?></p>
        </div>
      </div>
    </div>
    <!-- Header Area -->
    <article>
      <div class="news-single__content-container">
        <?php the_content(); ?>
        <?php if (get_field('show_form')) {
          ?>
          <div class="blog-single__bottom-form-container">
            <?php getForm(array('title' => "Subscribe to get the latest updates!", "content" => "blog")); ?>
          </div>
          <?php

        } ?>
      </div>
        <?php getSocialWidget(get_field('button_color')); ?>
    </article>
	</div>


<?php

}

/*
 ** Notice -- custom footer function
 *	$args of jpFooter() can be color, mono,
 *	orange, teal, or blue.
 **
 */
jpFooter(array('color' => 'color'));