<?php
function my_login_logo()
{ ?>
    <style type="text/css">
    @import url("https://fonts.googleapis.com/css?family=Fira+Mono:500|Lato:300,300i,400,400i,700,700i,900");

    body.login {
      font-family: "Lato";
      background: radial-gradient(643.68px at 50.03% 51.1%, rgba(255, 255, 255, 0.25) 0%, rgba(255, 255, 255, 0) 100%), linear-gradient(281.45deg, #0163A9 8.72%, #45A1DE 94.31%);
      transform: matrix(-1, 0, 0, 1, 0, 0);
    }
    p#nav a, p#backtoblog a {
    color: white !important;
    font-size: 13px;
    text-transform: uppercase;
}

    body > * {
      transform: matrix(-1, 0, 0, 1, 0, 0);
    }
    div#login h1 a {
    background-image: unset;
    display: none;
    padding-bottom: 30px;
  }

  div#login h1 {
    background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.svg);
    height:65px;
    width:320px;
    background-size: 320px 65px;
    background-repeat: no-repeat;
    margin-bottom: 60px;
    margin-top: 30px;
  }
  form#loginform {
    border-radius: 4px;
    box-shadow: 3px 5px 26px rgba(0, 0, 0, 0.08), 1px 1px 7px rgba(0, 0, 0, 0.04);
    background-color: #fefefe;
  }
  .login label {
    color: #585858 !important;
    font-weight: 400;
    font-size: 13px !important;
    text-transform: uppercase;
  }

  .login form .input, .login input[type=text] #user_login,
  .login form .input, .login input[type=text] #user_pass {
    font-size: 18px;
    width: 100%;
    padding: 6px 10px;
    margin: 4px 6px 28px 0px;
    border-radius: 4px;
}

.wp-core-ui .button-primary#wp-submit {
    background: linear-gradient(109.2deg, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0) 97.53%), #FF6600;
    border-color: transparent;
    box-shadow: 2px 4px 12px rgba(0, 0, 0, 0.12);
    color: #fff;
    text-decoration: none;
    text-shadow: none;
    border-radius: 4px;
    height: unset;
    padding: 4px 30px;
    font-size: 14px;
    text-transform: uppercase;
    letter-spacing: -0.015em;
    font-weight: 600;
    margin-top: 35px;
    margin-bottom: -24px;
}

.login #backtoblog, .login #nav {
    font-size: 13px;
    padding: 0 24px;
    text-align: center;
}

p#backtoblog {
    margin: 12px 24px 0 15px;
}


    </style>
<?php

}
add_action('login_enqueue_scripts', 'my_login_logo');