<?php
if (function_exists('acf_add_options_page')) {
  // add parent
  acf_add_options_page(array(
    'page_title' => 'Theme Options',
    'menu_options' => 'Theme Options',
    'menu_slug' => 'theme-options',
    'capability' => 'edit_posts',
    'parent_slug' => '',
    'position' => '50.1',
    'icon_url' => 'dashicons-editor-table'
  ));

  // add sub page
  acf_add_options_sub_page(array(
    'page_title' => 'Header',
    'menu_title' => 'Header',
    'menu_slug' => 'theme-options-header',
    'parent_slug' => 'theme-options',
    'capability' => 'edit_posts',
    'position' => 'false',
    'icon_url' => 'false'
  ));

  // add sub page
  acf_add_options_sub_page(array(
    'page_title' => 'Footer',
    'menu_title' => 'Footer',
    'menu_slug' => 'theme-options-footer',
    'parent_slug' => 'theme-options',
    'capability' => 'edit_posts',
    'position' => 'false',
    'icon_url' => 'false'
  ));

  acf_add_options_sub_page(array(
    'page_title' => 'Documentation Options',
    'menu_title' => 'Docs Options',
    'menu_slug' => 'documentation-page',
    'parent_slug' => 'edit.php?post_type=documentation',
    'capability' => 'edit_posts',
    'position' => 'false',
    'icon_url' => 'false'
  ));
}