<?php
function getBlogAnnouncement($post, $index)
{
  // The Announcement Template for the blog
  ?>
    <div class="blog-post card-container announcement" style="order: <?php echo $index ?>">
      <div class="blog-post annoucement <?php echo "background--" . ($post['color']) . "800"; ?>">
        <article class="blog-post__announcement">
            <span class="announcement-summary"><?php echo $post['summary']; ?></span>
            <span class="announcement-summary overline"><a href="<?php echo $post['permalink']; ?>"><?php echo $post['cta']; ?></a></span>
        </article>
      </div>
    </div>
  <?php

}