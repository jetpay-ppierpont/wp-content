<?php
function getBlogChangelog($changelogs, $postKey, $index)
{
  $postKey = strtotime('-1 month', $postKey);
  // The Changelog Template for the blog

  /*
   **
   * This is the Header and Container of the changelog Summary Card
   **
   */
  ?>
    <div class="blog-post card-container announcement changelog" style="order: <?php echo $index ?>">
      <div class="blog-post annoucement changelog">
        <article class="changelog-content_container">
          <a href="<?php echo $changelogs[0]['archive'] ?>">
          <h3 class="overline color--gray700 changelog" style="font-weight: 600;">Changelog<i class="fas fa-dice-d6 color--blue500" style="padding: 0 .8em; font-size: 1.2em;"></i><span class="color--gray600"><?php echo date('F Y', $postKey); ?></span></h3>
          </a>
  <?php
  /*
   **
   * This is the body of the changelog, below the header
   **
   */
  foreach ($changelogs as $key => $changelog) {
    $changelog['product'] = getTitle($changelog['product']);
    $date = strtotime($changelog['releaseDate']);

    ?>
          <article class="changelog-entry">
            <div class="changelog-title__container">
              <a href="<?php echo $changelog['permalink'] ?>">
                <p class="overline">
                  <?php echo $changelog['product'] ?>
                </p>
              </a>
            </div>
            <div class="changelog-summary__container">
              <p class="color--gray600">
                <?php echo $changelog['summary']; ?>
              </p>

              <a href="<?php echo $changelog['permalink']; ?>">
              <p class="caption color--gray600">
               Read More <i class="fas fa-long-arrow-alt-right"></i>
              </p>
              </a>
            </div>
          </article>
    <?php

  }
  ?>
        </article>
      </div>
    </div>
  <?php

}