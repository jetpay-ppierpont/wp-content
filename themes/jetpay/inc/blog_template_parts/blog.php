<?php
function getBlogPost($post, $index)
{
  // The BlogPost Template for the blog
  ?>
  <div class="blog-post card-container" style="order: <?php echo $index ?>">
    <div class="blog-post card">
      <article class="blog-post__container">
        <a href="<?php echo $post['archive'] ?>">
          <h4 class="color--gray500 archive-link">Blog</h4>
        </a>
        <a class="normal-link" href="<?php echo $post['permalink']; ?>">
          <h3 class="card-title blog-post color--gray700">
            <?php echo $post['title']; ?>
          </h3>
        </a>
        <span class="meta-info__container">
        <p class="author-name"><?php echo $post['author'] ?></p>
        <p class="date caption spacing-medium-small"><?php echo $post['date'] ?></p>
        </span>

        <hr class="smooth spacing-medium">
        <span class="user-content">
          <?php echo wpautop($post['content']); ?>
        </span>
      </article>
    </div>
  </div>
  <?php

}

function getSocialWidget($color)
{
  $colorClass = "color--" . strtolower($color) . "700";
  ?>

    <p class="sidebar-header"><strong>Share</strong></p>
    <div>
      <a href="<?php echo "https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/sharer/sharer.php?u=http%3A" . get_the_permalink(); ?>">
        <div class="blog-single__sidebar-social">
          <span class="<?php echo $colorClass ?>">
            <i class="fab fa-facebook-f"></i>
          </span>
          <p class="color--dark-secondary">Facebook</p>
        </div>
      </a>
      <a href="<?php echo "https://twitter.com/home?status=http%3A" . get_the_permalink(); ?>">
        <div class="blog-single__sidebar-social">
          <span class="<?php echo $colorClass ?>">
          <i class="fab fa-twitter"></i>
          </span>
          <p class="color--dark-secondary">Twitter</p>
        </div>
      </a>
      <a href="<?php echo "https://www.linkedin.com/shareArticle?mini=true&url=https%3A" . get_the_permalink() . "&title=" . urlencode(get_the_title()) . "&summary=" . get_field('social_media_summary') . "source=JetPay.com" ?>">
        <div class="blog-single__sidebar-social">
          <span class="<?php echo $colorClass ?>">
            <i class="fab fa-linkedin-in"></i>
          </span>
          <p class="color--dark-secondary">Linkedin </p>
        </div>
      </a>
    </div>
  <?php

}

function getRelatedBlogs($category, $product, $ID)
{
  $args = array(
    'posts_per_page' => 4,
    'post_type' => 'blog', // the type of post that we are querying
    'meta_query' => array(
      'relation' => 'or',
      array(
        'key' => 'related_product',
        'value' => $product,
        'compare' => 'LIKE',
      ),
      array(
        'key' => 'blog_category',
        'value' => $category,
        'compare' => 'LIKE',
      ),
    ),
  );
  $relatedCategoryBlogs = new WP_Query($args);
  ?>
        <p class="sidebar-header"><strong>Related Blogs</strong></p>
        <div class="related-blogs">
        <?php
        $i = 0;
        while ($relatedCategoryBlogs->have_posts()) : $relatedCategoryBlogs->the_post();
        // the index ($i) is making sure we have 3 and we can skip posts that are the same index as the current post...
        if ($i < 3 && get_the_ID() != $ID) { ?>
            <p class="caption"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></p>

        <?php $i++;
      }
      endwhile; ?>
        </div>
  <?php
  wp_reset_postdata();
}