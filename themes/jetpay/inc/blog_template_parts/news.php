<?php

function getNewsArticle($article, $index)
{
   // The BlogPost Template for the News Article
  ?>
   <div class="blog-post card-container" style="order: <?php echo $index ?>">
     <div class="blog-post card news">
       <article class="blog-post__container">
         <a href="<?php echo $article['archive'] ?>">
          <h4 class="archive-link">
            News
          </h4>
         </a>
         <a class="normal-link" href="<?php echo $article['permalink']; ?>">
           <h3 class="card-title blog-post color--gray700">
             <?php echo $article['title']; ?>
           </h3>
         </a>
         <span>
         <p class="caption spacing-medium-small"><?php echo $article['date'] ?></p>
         </span>

         <hr class="smooth spacing-medium">
         <!-- <p class="card-subtitle no-margin blog-post"><?php echo $article['author']; ?></p> -->
         <span class="">
           <?php echo $article['excerpt']; ?>
           <a href="<?php echo $article['archive'] ?>">
             <p style="text-align: right;" class="overline link-normal spacing-small">Read More <i class="far fa-long-arrow-alt-right"></i></p>
           </a>
         </span>
       </article>
     </div>
   </div>
   <?php

}