<?php

function getTitle($args)
{
  $productPost = get_post($args);
  $title = isset($productPost->post_title) ? $productPost->post_title : '';
  return $title;
}

// this function outputs the date of the changelog if it's different from the previous post.
function headerDate($wp_query)
{

  $prevPost = get_previous_post(true);
  $prevRawDate = get_field('date', $prevPost->ID);
  $rawDate = get_field('date');
  $currentDate = date("F Y", strtotime($rawDate));
  $prevDate = date("F Y", strtotime($prevRawDate));
  if ($wp_query->current_post === 0) {

    ?>
    <h4 class="new-month-title color--dark-secondary"><?php echo $currentDate ?></h4>
    <div class="changelog-section">
  <?php

} else {
  if ($currentDate != $prevDate) { ?>
    </div>
      <h4 class="new-month-title color--dark-secondary"> <?php echo $currentDate ?></h4>
        <?php if (get_next_post()->post_title) { ?>
        <div class="changelog-section">
        <?php

      }

    }
  }
}

function convertColor($args)
{
  echo 'var(--' . $args . ')';
}

/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function jetpayReadMore($more)
{
  return '';
}
add_filter('excerpt_more', 'jetpayReadMore');

/*
The next few functions are to disable
the default post type'
 */
add_action('admin_menu', 'remove_default_post_type');

function remove_default_post_type()
{
  remove_menu_page('edit.php');
}
add_action('admin_bar_menu', 'remove_default_post_type_menu_bar', 999);

function remove_default_post_type_menu_bar($wp_admin_bar)
{
  $wp_admin_bar->remove_node('new-post');
}
add_action('wp_dashboard_setup', 'remove_draft_widget', 999);

function remove_draft_widget()
{
  remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
}

/**
 * Replaces paragraph elements with double line-breaks.
 *
 * This is the inverse behavior of the wpautop() function
 * found in WordPress which converts double line-breaks to
 * paragraphs. Handy when you want to undo whatever it did.
 *
 * @see    wpautop()
 *
 * @param  string $pee
 * @param  bool   $br (optional)
 *
 * @return string
 */
function unautop( $pee, $br = true ) {
	// Match plain <p> tags and their contents (ignore <p> tags with attributes)
	$matches = preg_match_all( '/<(p+)*(?:>(.*)<\/\1>|\s+\/>)/m', $pee, $pees );
	if ( ! $matches ) {
		return $pee;
	}
	$replace = array( "\n" => '', "\r" => '' );
	if ( $br ) {
		$replace['<br>']   = "\r\n";
		$replace['<br/>']  = "\r\n";
		$replace['<br />'] = "\r\n";
	}
	foreach ( $pees[2] as $i => $tinkle ) {
		$replace[ $pees[0][ $i ] ] = $tinkle . "\r\n\r\n";
	}
	return rtrim(
		str_replace(
			array_keys( $replace ),
			array_values( $replace ),
			$pee
		)
	);
}

// This let's us do custom excerpt lengths

function excerpt($limit)
{
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt) >= $limit) {
    array_pop($excerpt);
    $excerpt = implode(" ", $excerpt) . '...';
  } else {
    $excerpt = implode(" ", $excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`', '', $excerpt);
  return $excerpt;
}

function content($limit, $content = null)
{
  $words = $content ? $content : unautop(get_the_content());
  $content = explode(' ', unautop(get_the_content()), $limit);
  if (count($content) >= $limit) {
    array_pop($content);
    $content = implode(" ", $content) . '...';
  } else {
    $content = implode(" ", $content);
  }
  $content = preg_replace('/[.+]/', '', $content);
  $content = apply_filters('the_content', $content);
  $content = wpautop(str_replace(']]>', ']]&gt;', $content));
  return $content;
}

function getFacebookTags($args = null)
{
  ?>
<meta property="og:url"                content="<?php echo get_the_permalink() ?>" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="<?php echo get_the_title() ?>" />
<meta property="og:description"        content="<?php echo get_field('social_media_summary') ?>" />
<meta property="og:image"              content="<?php echo get_field('facebook_sharing_image') ?>" />
<?php

}

