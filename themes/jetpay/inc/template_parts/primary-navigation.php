<?php
function primaryNavigation()
{
  ?>

    <?php
    $menu_name = 'primary_navigation';
    $locations = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object($locations[$menu_name]);
    $menuitems = wp_get_nav_menu_items($menu->term_id, array('order' => 'DESC'));
    $fullMenu = [];
    ?>

      <?php
      $count = 0;
      $submenu = false;
      foreach ($menuitems as $item) :
        $link = $item->url;
      $title = $item->title;
          // item does not have a parent so menu_item_parent equals 0 (false)
      if (!$item->menu_item_parent) :
          // save this id for later comparison with sub-menu items
      $parent_id = $item->ID;

      $fullMenu["parents"]["$parent_id"] = [
        'id' => $parent_id,
        'title' => $title,
        'link' => $link
      ];
      $fullMenu["children"]["$parent_id"] = [];
      endif; ?>

          <?php if ($parent_id == $item->menu_item_parent) : ?>

        <?php if (!$submenu) : $submenu = true; ?>
        <?php endif;

        $fullMenu['children']["$parent_id"][] = [
          'title' => $title,
          'link' => $link
        ];

        if ($menuitems[$count + 1]->menu_item_parent != $parent_id && $submenu) : ?>
        <?php $submenu = false;
        endif;
        // End Main

        endif; ?>

      <?php if ($menuitems[$count + 1]->menu_item_parent != $parent_id) :;
      // End Here
      $fullMenu["menuIcon"] = get_field('menu_button_icon', 'options');
      $fullMenu["searchIcon"] = get_field('search_button_icon', 'options');
      $submenu = false;
      endif; ?>

    <?php $count++;
    endforeach; ?>

  <script>
    var wpMenu = <?php echo json_encode($fullMenu); ?>
  </script>
<?php

}