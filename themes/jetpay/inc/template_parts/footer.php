<?php

function getFooter($args = null)
{
  ?>

</div>
</main>
<footer>
  <div class="container">
    <div class="modal__overlay"></div>
    <div class="footer">
      <?php wp_nav_menu(array(
          'container' => 'div', 'container_class' => 'footer-box', 'menu_class' => 'footer-box__list',
          'echo' => true, 'fallback_cb' => 'wp_page_menu', 'before' => '', 'after' => '', 'link_before' => '<p>', 'link_after' => '</p>', 'items_wrap' => '<ul class="%2$s">%3$s</ul>', 'item_spacing' => 'preserve',
          'depth' => 0, 'walker' => '', 'theme_location' => 'footer_menu'
        )) ?>
    </div>
  </div>
  <div class="bottom-footer">
    <div class="jetpay-disclaimer">
      <p class="caption"><?php the_field('jetpay_disclaimer_text', 'option'); ?></p>
    </div>
    <div class="ncr-footer-logo"></div>
  </div>
  <?php
  require "primary-navigation.php";
  primaryNavigation();
  require "search-vars.php";
  searchVars();
  wp_footer();
  ?>

</footer>
<?php
echo "</body>";
}