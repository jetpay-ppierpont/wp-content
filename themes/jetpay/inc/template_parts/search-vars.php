<?php

function searchVars()
{
  ?>
  <script>
		var wpFormEntryResponse = "<?php if (get_field('success_message')) {
                              echo get_field('success_message');
                            } else {
                              echo "Thank you for submitting this form!";
                            } ?>";
		var wpPostType = "<?php echo get_post_type(); ?>";
		var wpPageTitle = "<?php echo get_the_title(); ?>";
		var wpId = "<?php echo get_the_ID(); ?>";
		var wpSite = "<?php echo get_site_url() ?>";
		// show form?
		var wpFormId = "<?php
		// If the form is "custom"
                  if (get_field('hubspot_form') == "custom") {
			// If the custom form is defined, use that
                    if (get_field('hubspot_form_id')) {
                      echo get_field('hubspot_form_id');
                    } else {
				// if the custom form is not defined, use the default
                      echo $args['form'];
                    };
			// if the form is not custom...
                  } else {
			// if the form is defined
                    if (get_field('hubspot_form')) {
				// use the custom form
                      echo get_field('hubspot_form');
                    } else {
				// otherwise, use the default form..
                      echo $args['form'];
                    }
                  }

                  ?>"
		</script>
  <?php

}