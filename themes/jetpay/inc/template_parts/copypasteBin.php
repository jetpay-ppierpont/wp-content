  <!-- Normal Blog Post -->
  <div class="blog-post card-container">
    <div class="blog-post card">
      <article class="blog-post__container">
        <h3 class="card-title blog-post"><a class="normal-link" href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
        <h5 class="card-subtitle blog-post"><?php get_the_author_nickname(); // author ?></h5>
        <span><?php the_content(); ?></span>
      </article>
    </div>
  </div>

<!-- Normal Changelog Summary -->
    <div class="changelog-container">
      <article class="changelog-entry">
        <a class="changelog normal-link" href="<?php the_permalink() ?>">
          <p class="changelog-entry-title overline">
            <span class="caption"><?php echo date("m.d", strtotime($date)); ?></span>
            <?php echo " – " . getTitle($product); ?>

          </p>

            <p class="view-changelog caption">View<i class="changelog far fa-external-link"></i></p>
          </a>

        </article>
    </div>



<!-- Updated Changelog Summary Interior -->
            <a class="changelog normal-link" href="<?php echo $changelog['permalink'] ?>">
              <p class="changelog-entry-title overline">
                <?php echo getTitle($changelog['product']); ?>
                <br>
                <span class="code"><?php echo date("m.d", strtotime($changelog['date'])); ?></span>
              </p>
              <p class="view-changelog caption">Read More</p>
            </a>
