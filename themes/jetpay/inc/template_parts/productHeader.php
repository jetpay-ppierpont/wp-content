<?php
require "moduleHeader.php";

function productHeader($category, $home = null)
{
  $currentID = get_the_ID();
  function currentClass($id)
  {
    return ($id == $currentID) ? 'current' : '';
  }

  $getProducts = new WP_Query(array(
    'posts_per_page' => 10,
    'post_type' => 'solutions', // the type of post that we are querying
    'meta_key' => 'category',
    'order' => 'ASC',
    'meta_query' => array(
      array(
        'key' => 'category',
        'compare' => '=',
        'value' => $category,
      ),
    )
  ));
  ?>

<div class="product-header">
  <div class="product-header__solution">
    <?php
      $inc = 1;
      while ($getProducts->have_posts()) : $getProducts->the_post(); ?>

    <a href="<?php the_permalink(); ?>" style="<?php echo $currentID == get_the_ID() ? 'opacity: .7;' : '' ?>">
      <p class="button-text"><?php the_title(); ?></p>
    </a>

    <?php if (((!$getProducts->current_post + 1) == ($getProducts->post_count)) and (($getProducts->post_count) !== (1))) {
        ?>
    <div class="button-text separator" style="padding-left: 0; padding-right: 0;">
      |
    </div>
    <?php

    }
    endwhile; ?>
  </div>
</div>


<?php
        if ($home == null) {
          ?>

<div class="container">
  <div class="header-product">
    <a class="product-header-back button-text" href="/<?php echo $category; ?>">
      <?php echo '<i class="fal fa-long-arrow-left"></i>  ' . "&nbsp;" . $category ?>
    </a>
    <div class="product-header-related">
      <?php getRelatedModules($currentID) ?>
    </div>
  </div>
</div>
<?php

    }
    ?>

<?php
  wp_reset_postdata();
}