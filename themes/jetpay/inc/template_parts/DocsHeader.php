<?php
function getDocsHeader()
{
  $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  ?>
  <!DOCTYPE html>
  <html  <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Workforce Today Docs | JetPay</title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
      <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <div id="main_container">
      <section id="sidebar">
        <div>
          <div class="logo">
            <a class="main" href="/"><?php Logo(array('size' => '100', 'color' => 'darkPrimary')); ?></a>
            <a class="docs" href="/docs"></a>
          </div>
          <div class="logo-border"></div>
        </div>
        <nav>

          <h2>
            <a href="/docs"><i class="fal fa-home"></i>Home</a>
          </h2>

          <?php // check if the flexible content field has rows of data
          if (have_rows('docs_menu', 'option')) :

          // loop through the rows of data
          while (have_rows('docs_menu', 'option')) : the_row();
          // index menus - so we kan do the link arrays below for each h2 tag's class name separately.
          $menu_index = get_row_index();
          $links_in_section["$menu_index"];

          $overview = get_the_permalink(get_sub_field('overview_page')[0]);
          $links_in_section["$menu_index"][] = $overview;
          if (get_row_layout() == 'menu_category') :

           /*
           *
           *This is all just to get an array of links...
           *
           */

          while (have_rows('category_subsections')) : the_row();
          while (have_rows('subheader_children')) : the_row();
          $links_in_section["$menu_index"][] = get_the_permalink(get_sub_field('docs_page_link'));
          if (in_array($actual_link, $links_in_section["$menu_index"], true)) {
            $check_active["$menu_index"] = 'active';
          }
          endwhile;
          endwhile;
            /* end */

          ?>




              <h2
                class="collapsible <?php echo $check_active["$menu_index"]; ?>"
                data-category="<?php the_sub_field('category') ?>"
                data-product="<?php the_sub_field('product') ?>">
                <a><i class="<?php the_sub_field('category_icon'); ?>"></i><?php the_sub_field('category_title'); ?></a>
              </h2>

              <?php // test for subcategories (nested) ?>
              <?php if (have_rows('category_subsections')) : ?>
                <ul class="collapsible-content swup-link">
                <?php // start loop #2 (nested) ?>
                <?php while (have_rows('category_subsections')) : the_row();
                $overviewLinkClass = '';
                if ($actual_link == $overview) {
                  $overviewLinkClass = 'current';
                }
                ?>
                  <!-- Show the overview -->
                    <li>
                      <a class="<?php echo $overviewLinkClass ?> swup-link" href="<?php echo $overview ?>">
                        <p class="caption">Overview</p>
                      </a>
                    </li>

                  <!-- Show the Subcategory Heading -->
                  <li
                    class="overline"
                    data-category="<?php the_sub_field('subcategory'); ?>"
                    data-module="<?php the_sub_field('related_product'); ?>"
                    >
                        <?php the_sub_field('subsection_title'); ?>
                  </li>

                  <?php while (have_rows('subheader_children')) : the_row(); ?>
                  <?php
                  $linkClass = '';
                  $theLink = get_the_permalink(get_sub_field('docs_page_link'));
                  if ($actual_link == $theLink) {
                    $linkClass = 'current';
                  }
                  ?>
                    <li>
                      <a href="<?php echo $theLink ?>" class="<?php echo $linkClass ?> swup-link" data-id="<?php echo get_sub_field('docs_page_link');; ?>">
                        <p class="caption"><?php echo get_the_title(get_sub_field('docs_page_link')) ?></p>

                      </a>
                    </li>

                  <?php endwhile; ?>

                <?php endwhile; ?>
                </ul>
              <?php // end loop #2 (nested) ?>

            <?php endif;

            endif;

            endwhile;

            else :

      // no layouts found

            endif ?>
        </nav>
      </section>
      <div id="scroll-container">

    <article id="content">
      <header id="content-header">
        <transition name="fade">
          <div id="overlay" v-on:click="show = null" v-if="show"></div>
        </transition>
        <nav v-on:blur="show = null">
          <div class="search-wrapper">
            <input class="seach-input" v-on:keyup="search" v-on:focus="show = true"  v-model="query" type="text"  placeholder="Search docs.." autofocus/>
          </div>
          <template v-if="show && results.length >= 1">
            <div id="search-results" role="listbox">
                <a class="results-item color--gray700" v-bind:href="/docs/ + result.slug" v-for="(result, index) in results" @click="show = false">
                  <div style="margin-bottom: 16px;">
                    <h6>{{ result.title.rendered }}</h6>
                    <div v-html="result.excerpt.rendered" class="excerpt-container"></div>
                  </div>
                </a>
            </div>
          </template>
          <transition name="shrink">
            <div v-if="!show" class="top-nav">
              <button class="orange button">Sign Up</button>
              <p class="caption">Contact</p>
              <p class="caption">Sign In</p>
            </div>
          </transition>
        </nav>
      </header>
  <?php

}

function getUrl()
{
  $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

  return $actual_link;
}

