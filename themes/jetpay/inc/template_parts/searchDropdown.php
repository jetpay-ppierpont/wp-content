<?php

function getSearchDropdown($args)
{
  ?>

  <li id="header-search" class="hidden"  tabindex="1" style="margin-left: 55px; z-index: 450;">
    <a class="paragraph no-margin" @click="enter()" title="search" style="color:<?php echo $args['linkColor']; ?>"><i class="far fa-search"></i></a>

    <transition name="grow" mode="out-in">
    <div v-on:click="leave()"  v-if="showOverlay" id="search-overlay"></div>
    </transition>

    <transition name="fade" mode="out-in">

      <div v-if="showOverlay" id="search-dropdown">

        <div v-cloak class="search-container">
          <i class="far fa-search"></i>
          <input id="search-field" v-model="query" class="search-input" v-on:keyup="updateQuery()" type="text" >

          <div class="search-result__container">
            <div v-for="(value, key) in results">

              <h4 class="search-result__category" style="text-transform: capitalize;">{{key}}</h4>
              <div class="search-result__item" v-for="val in value">
                <a v-bind:href="val.link">
                  <div id="search-result__for-hover">
                    <h6>{{val.title.rendered}}</h6>
                    <p v-html="val.excerpt.rendered" class="caption"></p>
                  </div>
                </a>
                </div>
            </div>
          </div>
        </div>
      </div>

    </transition>

  </li>

  <?php

}