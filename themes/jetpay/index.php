<?php getHeader(array('color' => 'darkSecondary')); ?>
<div class="blog-archive container">
<?php

while (have_posts()) {
  the_post(); ?>
  <div>
      <?php the_content() ?>
  </div>

<?php

} ?>

</div>

<?php
getFooter();
