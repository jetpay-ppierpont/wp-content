"use strict";

const webpack = require("webpack");
const autoprefixer = require("autoprefixer");
const AssetsPlugin = require("assets-webpack-plugin");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const FriendlyErrorsPlugin = require("friendly-errors-webpack-plugin");
const path = require("path");
const fs = require("fs");

// Make sure any symlinks in the project folder are resolved:
// https://github.com/facebookincubator/create-react-app/issues/637
const appDirectory = fs.realpathSync(process.cwd());

function resolveApp(relativePath) {
	return path.resolve(appDirectory, relativePath);
}

const paths = {
	appSrc: resolveApp("src"),
	appBuild: resolveApp("build"),
	appIndexJs: resolveApp("src/index.js"),
	editorJs: resolveApp("src/editor.js"),
	imagesJs: resolveApp("src/img/index.js"),
	appNodeModules: resolveApp("node_modules")
};

module.exports = {
	mode: "development",
	bail: false,
	// We generate sourcemaps in production. This is slow but gives good results.
	// You can exclude the *.map files from the build during deployment.
	target: "web",
	devtool: "cheap-eval-source-map",
	entry: {
		app: [paths.appIndexJs],
		editor: [paths.editorJs]
	},
	output: {
		path: paths.appBuild,
		filename: "[name]bundle.js"
	},
	resolve: {
		extensions: [".js", ".vue", ".json"],
		alias: {
			vue$: "vue/dist/vue.esm.js",
			"@": resolveApp("src")
		}
	},
	module: {
		rules: [
			// Disable require.ensure as it's not a standard language feature.
			{ parser: { requireEnsure: false } },

			// this will apply to both plain `.js` files
			// AND `<script>` blocks in `.vue` files
			{
				test: /\.vue$/,
				loader: "vue-loader"
			},
			// Transform ES6 with Babel
			{
				test: /\.js?$/,
				loader: "babel-loader",
				include: paths.appSrc
			},
			{
				test: /.scss$/,
				use: [
					"vue-style-loader",
					"css-loader",
					{
						loader: "sass-loader",
						options: {
							// you can also read from a file, e.g. `variables.scss`
							data: `$color: red;`
						}
					}
				]
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				loader: "file-loader",
				options: {
					name: "[name].[ext]",
					outputPath: "../images",
					publicPath: "/wp-content/themes/jetpay/images/"
				}
			},
			{
				test: /\.css$/,
				use: ["vue-style-loader", "css-loader"]
			}
		]
	},
	plugins: [
		new VueLoaderPlugin(),
		new MiniCssExtractPlugin({
			filename: "[name].css",
			chunkFilename: "[id].css"
		}),
		new webpack.EnvironmentPlugin({
			NODE_ENV: "development", // use 'development' unless process.env.NODE_ENV is defined
			DEBUG: false
		}),
		new AssetsPlugin({
			path: paths.appBuild,
			filename: "assets.json"
		}),

		new FriendlyErrorsPlugin({
			clearConsole: false
		}),

		new BrowserSyncPlugin({
			notify: false,
			host: "localhost",
			port: 4000,
			logLevel: "silent",
			files: ["./*.php"],
			proxy: "http://jetpay.development/"
		})
	].filter(Boolean)
};
