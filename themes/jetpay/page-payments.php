<?php
getHeader();
productHeader("payments", "home");
?>
<div>
<?php

while (have_posts()) {
  the_post(); ?>
  <div class="page-builder">
      <?php the_content() ?>
  </div>

<?php

} ?>

</div>

<?php
getFooter();
